# Changelog
All notable changes to this project will be documented in this file.

## [1.0.9] - 20-01-2020
### Changed
    - include generator in package

## [1.0.6] - 15-01-2020
### Updated
    - Update version v2.0.4
    - Unique key find methods(FindXxx) throws KeyNotFoundException when not found.You can use new TryFindXxx instead.
    - New MemoryDatabase.Validate() method validates data is valid. In default, check duplicate key in unique index table. IValidatable<T> to make custom validator logic.
```csharp
[MemoryTable("quest_master"), MessagePackObject(true)]
public class QuestMaster : IValidatable<QuestMaster>
{
    [PrimaryKey]
    public int QuestId { get; set; }
    public string Name { get; set; }
    public int RewardItemId { get; set; }
    public int Cost { get; set; }

    public void Validate(IValidator<QuestMaster> validator)
    {
        var itemMaster = validator.GetReferenceSet<ItemMaster>();

        // check external reference.
        itemMaster.Exists(x => x.RewardItemId, x => x.ItemId);

        // check value range
        validator.Validate(x => x.Cost >= 10);
        validator.Validate(x => x.Cost <= 50);

        // check all table data
        if (validator.CallOnce())
        {
            var quests = validator.GetTableSet();
            // check unique other than index keys
            quests.Unique(x => x.Name);
        }
    }
}
```
    - Add ValidatableSet.Where
    - Remove MetaMemoryDatabase
    - Validation message shows PK value.
	- ValidateResult.FailedResults returns struct FaildItem.



	
