﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEditor;

#if UNITY_EDITOR
public static partial class Builder
{
    private const string MEMORY_MASTER_GENERATOR = "/GeneratorTools";
    private const string MEMORY_MASTER_GENERATE_OUTPUT = "/Root/Scripts/Database/Generated/MemoryMaster.Generated";

    [MenuItem("Window/MemoryMaster/Generate")]
    private static void GenerateMemoryMaster()
    {
        UnityEngine.Debug.Log("MemoryMaster Generator : Start...");
        var rootPath = UnityEngine.Application.dataPath + "/..";
        // ReSharper disable once RedundantAssignment
        var exeFileName = string.Empty;
#if UNITY_EDITOR_WIN
        exeFileName = "/win-x64/MasterMemory.Generator.exe";
#elif UNITY_EDITOR_OSX
            exeFileName = "/osx-x64/MasterMemory.Generator";
#elif UNITY_EDITOR_LINUX
            exeFileName = "/linux-x64/MasterMemory.Generator";
#else
            return;
#endif
        var psi = new ProcessStartInfo()
        {
            CreateNoWindow = true,
            WindowStyle = ProcessWindowStyle.Hidden,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            FileName = Path.GetFullPath("Packages/com.yenmoc.master-memory") + MEMORY_MASTER_GENERATOR + exeFileName,
            Arguments = $@"-i ""{UnityEngine.Application.dataPath}"" -o ""{UnityEngine.Application.dataPath}{MEMORY_MASTER_GENERATE_OUTPUT}"" -n ""Database""",
        };

        var p = Process.Start(psi);
        if (p == null) return;
        p.EnableRaisingEvents = true;
        p.Exited += (sender, e) =>
        {
            var data = p.StandardOutput.ReadToEnd();
            UnityEngine.Debug.Log($"{data}");
            UnityEngine.Debug.Log("MemoryMaster Generator : Complete!");
            p?.Dispose();
            p = null;
        };

        void AddDefineSymbols(params string[] symbols)
        {
            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            var allDefines = definesString.Split(';').ToList();
            allDefines.AddRange(symbols.Except(allDefines));
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", allDefines.ToArray()));
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        AddDefineSymbols("MEMORY_MASTER");
    }
}
#endif